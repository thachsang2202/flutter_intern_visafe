import 'package:fitnessx/screen/onboarding/onboarding4.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Onboarding3 extends StatefulWidget {
  const Onboarding3({super.key});

  @override
  State<Onboarding3> createState() => _Onboarding3State();
}

class _Onboarding3State extends State<Onboarding3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
              width: MediaQuery.of(context).size.width,
              child: SvgPicture.asset(
                'assets/onboarding/onboarding3.svg',
                fit: BoxFit.fill,
              )),
          const SizedBox(
            height: 50.0,
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Eat Well',
                  style: TextStyle(
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w700,
                    fontSize: 30.0,
                  ),
                  textAlign: TextAlign.left,
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  "Let's start a healthy lifestyle with us, we can determine your diet every day. healthy eating is fun",
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      color: Color.fromRGBO(121, 109, 112, 1)),
                )
              ],
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const Onboarding4()));
        },
        elevation: 0,
        backgroundColor: Colors.transparent,
        child: SvgPicture.asset('assets/onboarding/button3.svg'),
      ),
    );
  }
}
