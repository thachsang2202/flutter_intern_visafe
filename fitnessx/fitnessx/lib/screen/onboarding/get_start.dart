import 'package:fitnessx/screen/onboarding/onboarding1.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

class GetStart extends StatefulWidget {
  const GetStart({super.key});

  @override
  State<GetStart> createState() => _GetStartState();
}

class _GetStartState extends State<GetStart> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [
            Color.fromRGBO(154, 196, 254, 1),
            Color.fromRGBO(146, 164, 253, 1)
          ])),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: [
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset('assets/onboarding/logo.svg'),
                  const SizedBox(
                    height: 10.0,
                  ),
                  const Text(
                    'Everybody Can Train',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        color: Color.fromRGBO(123, 111, 114, 1),
                        fontSize: 18.0),
                  )
                ],
              ),
            ),
            Positioned(
                bottom: 50.0,
                child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Onboarding1()));
                        },
                        child: Container(
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(50))),
                          height: 50.0,
                          width: 350.0,
                          child: Center(
                              child: Text(
                            'Get Started',
                            style: TextStyle(
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w600,
                                fontSize: 18.0,
                                foreground: Paint()
                                  ..shader = const LinearGradient(
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                      colors: [
                                        Color.fromRGBO(154, 196, 254, 1),
                                        Color.fromRGBO(146, 164, 253, 1)
                                      ]).createShader(
                                      Rect.fromLTWH(0.0, 0.0, 200.0, 70.0))),
                          )),
                        ),
                      ),
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
