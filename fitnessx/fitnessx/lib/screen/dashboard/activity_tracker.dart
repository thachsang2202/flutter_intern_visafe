import 'package:fitnessx/components/activity_card.dart';
import 'package:fitnessx/components/progress.dart';
import 'package:fitnessx/components/target.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';

class ActivityTracker extends StatefulWidget {
  const ActivityTracker({super.key});

  @override
  State<ActivityTracker> createState() => _ActivityTrackerState();
}

class _ActivityTrackerState extends State<ActivityTracker> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      height: 40,
                      width: 40,
                      decoration: const BoxDecoration(
                          color: Color.fromRGBO(247, 248, 248, 1),
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child:
                            SvgPicture.asset('assets/dashboard/arrow_left.svg'),
                      ),
                    ),
                  ),
                  const Text(
                    'Notification',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w700,
                        fontSize: 20.0),
                  ),
                  Container(
                    height: 40,
                    width: 40,
                    decoration: const BoxDecoration(
                        color: Color.fromRGBO(247, 248, 248, 1),
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child:
                          SvgPicture.asset('assets/dashboard/detail_navs.svg'),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20.0,
              ),
              Container(
                height: 150,
                width: 400,
                decoration: const BoxDecoration(
                    color: Color.fromRGBO(224, 229, 244, 1),
                    borderRadius: BorderRadius.all(Radius.circular(15.0))),
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            'Today Target',
                            style: TextStyle(
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.w600,
                                fontSize: 15.0),
                          ),
                          Container(
                            height: 30,
                            width: 30,
                            decoration: const BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                gradient: LinearGradient(colors: [
                                  Color.fromRGBO(157, 206, 255, 1),
                                  Color.fromRGBO(146, 163, 253, 1)
                                ])),
                            child: const Icon(
                              Icons.add,
                              color: Colors.white,
                              size: 20,
                            ),
                          )
                        ],
                      ),
                      const Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Target(
                              value: '8L',
                              title: 'Water Intake',
                              image: 'assets/dashboard/water.svg'),
                          Target(
                              value: '2400',
                              title: 'Foot Steps',
                              image: 'assets/dashboard/boots.svg')
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    'Activity Progress',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 18,
                        fontWeight: FontWeight.w600),
                  ),
                  Container(
                    width: 90,
                    height: 40,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(20),
                        ),
                        gradient: LinearGradient(colors: [
                          Color.fromRGBO(157, 206, 255, 1),
                          Color.fromRGBO(146, 163, 253, 1)
                        ])),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            'Weekly',
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Poppins',
                            ),
                          ),
                          SvgPicture.asset('assets/dashboard/arrow_down.svg')
                        ],
                      ),
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20.0,
              ),
              Container(
                width: 500,
                height: 270,
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 3,
                          blurRadius: 7,
                          offset: const Offset(0, 3))
                    ],
                    borderRadius:
                        const BorderRadius.all(Radius.circular(15.0))),
                child: const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Progress(
                            title: 'Sun', isTypeColorLight: true, percent: 20),
                        Progress(
                            title: 'Mon', isTypeColorLight: false, percent: 50),
                        Progress(
                            title: 'Tue', isTypeColorLight: true, percent: 70),
                        Progress(
                            title: 'Wed', isTypeColorLight: false, percent: 30),
                        Progress(
                            title: 'Thu', isTypeColorLight: true, percent: 40),
                        Progress(
                            title: 'Fri', isTypeColorLight: false, percent: 70),
                        Progress(
                            title: 'Sat', isTypeColorLight: true, percent: 90),
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
              const Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Latest Activity',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 18,
                        fontWeight: FontWeight.w600),
                  ),
                  Text(
                    'See more',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        color: Color.fromRGBO(173, 164, 165, 1)),
                  )
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              const Column(
                children: [
                  ActivityCard(
                      image: 'assets/dashboard/drinking.svg',
                      title: 'Drinking 300ml Water',
                      time: 'About 3 minutes ago'),
                  ActivityCard(
                      image: 'assets/dashboard/eat_snack.svg',
                      title: 'Eat Snack (Fitbar)',
                      time: 'About 10 minutes ago'),
                  ActivityCard(
                      image: 'assets/dashboard/drinking.svg',
                      title: 'Drinking 300ml Water',
                      time: 'About 3 minutes ago'),
                  ActivityCard(
                      image: 'assets/dashboard/eat_snack.svg',
                      title: 'Eat Snack (Fitbar)',
                      time: 'About 10 minutes ago'),
                  ActivityCard(
                      image: 'assets/dashboard/drinking.svg',
                      title: 'Drinking 300ml Water',
                      time: 'About 3 minutes ago'),
                  ActivityCard(
                      image: 'assets/dashboard/eat_snack.svg',
                      title: 'Eat Snack (Fitbar)',
                      time: 'About 10 minutes ago'),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
