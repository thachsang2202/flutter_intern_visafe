import 'package:fitnessx/components/notification_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({super.key});

  @override
  State<NotificationScreen> createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    height: 40,
                    width: 40,
                    decoration: const BoxDecoration(
                        color: Color.fromRGBO(247, 248, 248, 1),
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child:
                          SvgPicture.asset('assets/dashboard/arrow_left.svg'),
                    ),
                  ),
                ),
                const Text(
                  'Notification',
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w700,
                      fontSize: 20.0),
                ),
                Container(
                  height: 40,
                  width: 40,
                  decoration: const BoxDecoration(
                      color: Color.fromRGBO(247, 248, 248, 1),
                      borderRadius: BorderRadius.all(Radius.circular(15))),
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: SvgPicture.asset('assets/dashboard/detail_navs.svg'),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20.0,
            ),
            const NotificationCard(
                backgroundColor: Color.fromRGBO(146, 163, 253, 1),
                title: 'Hey, it’s time for lunch',
                time: 'About 1 minutes ago',
                image: 'assets/dashboard/pancake1.svg'),
            const SizedBox(
              height: 20.0,
            ),
            const NotificationCard(
                backgroundColor: Color.fromRGBO(146, 163, 253, 1),
                title: 'Don’t miss your lowerbody',
                time: 'About 2 minutes ago',
                image: 'assets/dashboard/charater1.svg'),
            const SizedBox(
              height: 20.0,
            ),
            const NotificationCard(
              backgroundColor: Color.fromRGBO(146, 163, 253, 1),
              title: 'Hey, it’s time for lunch',
              time: 'About 1 minutes ago',
            ),
            const SizedBox(
              height: 20.0,
            ),
            const NotificationCard(
                backgroundColor: Color.fromRGBO(146, 163, 253, 1),
                title: 'Congratulations, You have',
                time: 'About 1 minutes ago',
                image: 'assets/dashboard/pancake1.svg'),
            const SizedBox(
              height: 20.0,
            ),
            const NotificationCard(
                backgroundColor: Color.fromRGBO(146, 163, 253, 1),
                title: 'Hey, it’s time for lunch',
                time: 'About 1 minutes ago',
                image: 'assets/dashboard/pancake1.svg'),
            const SizedBox(
              height: 20.0,
            ),
            const NotificationCard(
                backgroundColor: Color.fromRGBO(146, 163, 253, 1),
                title: 'Hey, it’s time for lunch',
                time: 'About 1 minutes ago',
                image: 'assets/dashboard/pancake1.svg'),
          ],
        ),
      ),
    );
  }
}
