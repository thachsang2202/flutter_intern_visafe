import 'package:fitnessx/components/workout.dart';
import 'package:fitnessx/screen/dashboard/activity_tracker.dart';
import 'package:fitnessx/screen/dashboard/notification_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      'Welcome Back,',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          color: Color.fromRGBO(173, 164, 165, 1)),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Stefani Wong',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w700,
                          fontSize: 25.0),
                    ),
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NotificationScreen()));
                  },
                  child: Container(
                    height: 50,
                    width: 50,
                    decoration: const BoxDecoration(
                        color: Color.fromRGBO(29, 22, 23, 0.07),
                        borderRadius: BorderRadius.all(Radius.circular(15.0))),
                    child: Center(
                      child: SvgPicture.asset(
                        'assets/dashboard/notification_active.svg',
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 20.0,
            ),
            Stack(
              children: [
                Container(
                  height: 150,
                  width: 400,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    gradient: LinearGradient(colors: [
                      Color.fromRGBO(157, 206, 255, 1),
                      Color.fromRGBO(146, 163, 253, 1),
                    ], begin: Alignment.centerLeft, end: Alignment.centerRight),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              'BMI (Body Mass Index)',
                              style: TextStyle(
                                  fontFamily: 'Poppins',
                                  color: Colors.white,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(
                              height: 5.0,
                            ),
                            const Text(
                              'You have a normal weight',
                              style: TextStyle(
                                  fontFamily: 'Poppins', color: Colors.white),
                            ),
                            const SizedBox(
                              height: 10.0,
                            ),
                            Container(
                              decoration: const BoxDecoration(
                                  gradient: LinearGradient(
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                      colors: [
                                        Color.fromRGBO(238, 164, 206, 1),
                                        Color.fromRGBO(197, 139, 242, 1),
                                      ]),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(50))),
                              height: 50.0,
                              width: 150.0,
                              child: const Center(
                                  child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      'View more',
                                      style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w600,
                                          fontSize: 18.0,
                                          color: Colors.white),
                                    ),
                                  ),
                                ],
                              )),
                            ),
                          ],
                        ),
                        Container(
                          height: 90,
                          width: 90,
                          decoration: const BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/dashboard/banner_pie.png'))),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 130,
                  width: 350,
                  child: SvgPicture.asset(
                    'assets/dashboard/banner_dots.svg',
                    fit: BoxFit.cover,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 30.0,
            ),
            Container(
              height: 80,
              width: 400,
              decoration: const BoxDecoration(
                  color: Color.fromRGBO(234, 240, 255, 1),
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      'Today Target',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 18,
                          fontWeight: FontWeight.w600),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ActivityTracker()));
                      },
                      child: Container(
                        height: 40,
                        width: 100,
                        decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(20)),
                            gradient: LinearGradient(colors: [
                              Color.fromRGBO(157, 206, 255, 1),
                              Color.fromRGBO(146, 163, 253, 1)
                            ])),
                        child: const Center(
                          child: Text(
                            'Check',
                            style: TextStyle(
                                fontFamily: 'Poppins',
                                color: Colors.white,
                                fontSize: 18),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 40.0,
            ),
            const Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Latest Workout',
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w600,
                      fontSize: 20.0),
                ),
                Text(
                  'See more',
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      color: Color.fromRGBO(173, 164, 165, 1)),
                )
              ],
            ),
            const SizedBox(
              height: 40.0,
            ),
            const Workout(
              backgroundColor: Color.fromRGBO(224, 234, 255, 1),
              calor: 180,
              minutes: 20,
              title: 'Fullbody Workout',
              percent: 50,
            ),
            const SizedBox(
              height: 20.0,
            ),
            const Workout(
              backgroundColor: Color.fromRGBO(224, 234, 255, 1),
              calor: 200,
              minutes: 30,
              title: 'Lowerbody Workout',
              percent: 60,
              image: 'assets/dashboard/charater1.svg',
            ),
            const SizedBox(
              height: 20.0,
            ),
            const Workout(
              backgroundColor: Color.fromRGBO(224, 234, 255, 1),
              calor: 200,
              minutes: 30,
              title: 'Ab Workout',
              percent: 30,
              image: 'assets/dashboard/charater2.svg',
            )
          ],
        ),
      ),
    ));
  }
}
