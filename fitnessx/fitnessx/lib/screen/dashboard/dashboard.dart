import 'package:fitnessx/components/nav_bar.dart';
import 'package:fitnessx/models/nav_model.dart';
import 'package:fitnessx/screen/dashboard/activity_screen.dart';
import 'package:fitnessx/screen/dashboard/activity_tracker.dart';
import 'package:fitnessx/screen/dashboard/camera_screen.dart';
import 'package:fitnessx/screen/dashboard/home_screen.dart';
import 'package:fitnessx/screen/dashboard/profile_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DashBoard extends StatefulWidget {
  const DashBoard({super.key});

  @override
  State<DashBoard> createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  final homeNavKey = GlobalKey<NavigatorState>();
  final activityKey = GlobalKey<NavigatorState>();
  final cameraKey = GlobalKey<NavigatorState>();
  final profileKey = GlobalKey<NavigatorState>();
  int selectedTab = 0;
  List<NavModel> items = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    items = [
      NavModel(
        page: const HomeScreen(),
        navKey: homeNavKey,
      ),
      NavModel(
        page: const ActivityTracker(),
        navKey: activityKey,
      ),
      NavModel(
        page: const CameraScreen(),
        navKey: cameraKey,
      ),
      NavModel(
        page: const ProfileScreen(),
        navKey: profileKey,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (items[selectedTab].navKey.currentState?.canPop() ?? false) {
          items[selectedTab].navKey.currentState?.pop();
          return Future.value(false);
        } else {
          return Future.value(true);
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: IndexedStack(
          index: selectedTab,
          children: items
              .map((page) => Navigator(
                    key: page.navKey,
                    onGenerateInitialRoutes: (navigator, initialRoute) {
                      return [
                        MaterialPageRoute(builder: (context) => page.page)
                      ];
                    },
                  ))
              .toList(),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: Container(
            margin: const EdgeInsets.only(top: 50),
            height: 64,
            width: 64,
            decoration: const BoxDecoration(
                gradient: LinearGradient(colors: [
                  Color.fromRGBO(154, 196, 255, 1),
                  Color.fromRGBO(146, 165, 253, 1)
                ], begin: Alignment.centerLeft, end: Alignment.centerRight),
                borderRadius: BorderRadius.all(Radius.circular(40))),
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
              child: SvgPicture.asset('assets/dashboard/search_icon.svg'),
            )),
        bottomNavigationBar: NavBar(
          pageIndex: selectedTab,
          onTap: (index) {
            if (index == selectedTab) {
              items[index]
                  .navKey
                  .currentState
                  ?.popUntil((route) => route.isFirst);
            } else {
              setState(() {
                selectedTab = index;
              });
            }
          },
        ),
      ),
    );
  }
}
