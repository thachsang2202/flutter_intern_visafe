import 'package:fitnessx/components/card_value.dart';
import 'package:fitnessx/components/section.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool isOnNotification = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      height: 40,
                      width: 40,
                      decoration: const BoxDecoration(
                          color: Color.fromRGBO(247, 248, 248, 1),
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child:
                            SvgPicture.asset('assets/dashboard/arrow_left.svg'),
                      ),
                    ),
                  ),
                  const Text(
                    'Profile',
                    style: TextStyle(
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w700,
                        fontSize: 20.0),
                  ),
                  Container(
                    height: 40,
                    width: 40,
                    decoration: const BoxDecoration(
                        color: Color.fromRGBO(247, 248, 248, 1),
                        borderRadius: BorderRadius.all(Radius.circular(15))),
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child:
                          SvgPicture.asset('assets/dashboard/detail_navs.svg'),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20.0,
              ),
              Row(
                children: [
                  SizedBox(
                    height: 60,
                    width: 60,
                    child: SvgPicture.asset(
                      'assets/dashboard/drinking.svg',
                      height: 10,
                      width: 10,
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  const Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Stefani Wong',
                          style: TextStyle(
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w600),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          'Lose a Fat Program',
                          style: TextStyle(
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w400,
                              color: Color.fromRGBO(164, 169, 173, 1)),
                        )
                      ],
                    ),
                  ),
                  Container(
                    width: 90,
                    height: 40,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(20),
                        ),
                        gradient: LinearGradient(colors: [
                          Color.fromRGBO(157, 206, 255, 1),
                          Color.fromRGBO(146, 163, 253, 1)
                        ])),
                    child: const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Edit',
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Poppins',
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              const Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CardValue(
                    value: '180cm',
                    title: 'Height',
                  ),
                  CardValue(
                    value: '65kg',
                    title: 'Weight',
                  ),
                  CardValue(
                    value: '22yo',
                    title: 'Age',
                  )
                ],
              ),
              const SizedBox(
                height: 30.0,
              ),
              Container(
                height: 200,
                width: 400,
                padding: const EdgeInsets.all(20.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 3,
                          blurRadius: 7,
                          offset: const Offset(0, 3))
                    ],
                    borderRadius:
                        const BorderRadius.all(Radius.circular(15.0))),
                child: const Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Account',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 18,
                          fontWeight: FontWeight.w600),
                    ),
                    Section(
                        icon: 'assets/dashboard/profile.svg',
                        title: 'Personal Data'),
                    Section(
                        icon: 'assets/dashboard/document.svg',
                        title: 'Achievement'),
                    Section(
                        icon: 'assets/dashboard/graph.svg',
                        title: 'Activity History'),
                    Section(
                        icon: 'assets/dashboard/chart.svg',
                        title: 'Workout Progress'),
                  ],
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
              Container(
                height: 100,
                width: 400,
                padding: const EdgeInsets.all(20.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 3,
                          blurRadius: 7,
                          offset: const Offset(0, 3))
                    ],
                    borderRadius:
                        const BorderRadius.all(Radius.circular(15.0))),
                child: Column(
                  children: [
                    const Text(
                      'Notification',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 18,
                          fontWeight: FontWeight.w600),
                    ),
                    Row(children: [
                      SvgPicture.asset('assets/dashboard/notification.svg'),
                      const SizedBox(
                        width: 15.0,
                      ),
                      const Expanded(
                        child: Text(
                          'Pop-up Notification',
                          style: TextStyle(
                              color: Color.fromRGBO(123, 111, 114, 1),
                              fontFamily: 'Poppins',
                              fontSize: 15.0,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            isOnNotification = !isOnNotification;
                          });
                        },
                        child: Container(
                          height: 20,
                          width: 40,
                          padding: const EdgeInsets.symmetric(horizontal: 2.0),
                          decoration: const BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [
                                    Color.fromRGBO(238, 164, 206, 1),
                                    Color.fromRGBO(197, 139, 242, 1),
                                  ]),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(50))),
                          child: Row(
                            mainAxisAlignment: isOnNotification
                                ? MainAxisAlignment.start
                                : MainAxisAlignment.end,
                            children: [
                              Container(
                                height: 15,
                                width: 15,
                                decoration: const BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15))),
                              ),
                            ],
                          ),
                        ),
                      )
                    ])
                  ],
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
              Container(
                height: 165,
                width: 400,
                padding: const EdgeInsets.all(20.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 3,
                          blurRadius: 7,
                          offset: const Offset(0, 3))
                    ],
                    borderRadius:
                        const BorderRadius.all(Radius.circular(15.0))),
                child: const Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Other',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 18,
                          fontWeight: FontWeight.w600),
                    ),
                    Section(
                        icon: 'assets/dashboard/message.svg',
                        title: 'Contact us'),
                    Section(
                        icon: 'assets/dashboard/shield_done.svg',
                        title: 'Privacy Policy'),
                    Section(
                        icon: 'assets/dashboard/setting.svg', title: 'Setting'),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
