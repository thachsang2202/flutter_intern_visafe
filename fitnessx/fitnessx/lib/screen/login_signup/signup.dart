import 'dart:ffi';

import 'package:fitnessx/components/button.dart';
import 'package:fitnessx/components/input.dart';
import 'package:fitnessx/screen/login_signup/profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  bool isChecked = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Column(
              children: [
                const SizedBox(
                  height: 50.0,
                ),
                const Text(
                  'Hey there,',
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w500,
                      fontSize: 20.0),
                ),
                const SizedBox(
                  height: 5.0,
                ),
                const Text(
                  'Create an Account',
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w700,
                      fontSize: 25.0),
                ),
                const SizedBox(
                  height: 25.0,
                ),
                const Input(
                  icon: 'assets/login_signup/profile_icon.svg',
                  placeHolder: 'First name',
                  iconEnd: 'assets/login_signup/hide_password.svg',
                ),
                const SizedBox(
                  height: 15.0,
                ),
                const Input(
                  icon: 'assets/login_signup/profile_icon.svg',
                  placeHolder: 'Last name',
                ),
                const SizedBox(
                  height: 15.0,
                ),
                const Input(
                  icon: 'assets/login_signup/message_icon.svg',
                  placeHolder: 'Email',
                ),
                const SizedBox(
                  height: 15.0,
                ),
                const Input(
                  icon: 'assets/login_signup/lock_icon.svg',
                  placeHolder: 'Password',
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Checkbox(
                        value: isChecked,
                        onChanged: (bool? value) {
                          setState(() {
                            isChecked = value!;
                          });
                        },
                        activeColor: const Color.fromRGBO(168, 159, 160, 1),
                        focusColor: const Color.fromRGBO(168, 159, 160, 1),
                        side: const BorderSide(
                            color: Color.fromRGBO(168, 159, 160, 1)),
                      ),
                      Expanded(
                        child: RichText(
                            maxLines: 2,
                            text: const TextSpan(
                                style: TextStyle(
                                    fontFamily: 'Poppins',
                                    color: Color.fromRGBO(173, 164, 165, 1)),
                                children: [
                                  TextSpan(
                                      text: 'By continuing you accept our '),
                                  TextSpan(
                                      text: 'Privacy Policy',
                                      style: TextStyle(
                                          decoration:
                                              TextDecoration.underline)),
                                  TextSpan(text: ' and '),
                                  TextSpan(
                                      text: 'Term of Use',
                                      style: TextStyle(
                                          decoration:
                                              TextDecoration.underline)),
                                ])),
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 50,
                ),
              ],
            ),
            Column(
              children: [
                Button(
                  text: 'Register',
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Profile()));
                  },
                ),
                const Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Divider(
                          height: 1.0,
                          indent: 2.0,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        child: Text(
                          'Or',
                          style: TextStyle(fontFamily: 'Poppins'),
                        ),
                      ),
                      Expanded(
                        child: Divider(
                          height: 1.0,
                        ),
                      )
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 70,
                      height: 70,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(15)),
                          border: Border.all(
                              width: 1.0,
                              color: const Color.fromRGBO(221, 218, 218, 1))),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 20.0),
                        child: Container(
                          decoration: const BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/login_signup/google_logo.png'))),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 50.0,
                    ),
                    Container(
                      width: 70,
                      height: 70,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(15)),
                          border: Border.all(
                              width: 1.0,
                              color: const Color.fromRGBO(221, 218, 218, 1))),
                      child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 20.0),
                          child: SvgPicture.asset(
                              'assets/login_signup/facebook_logo.svg')),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Already have an account?',
                      style: TextStyle(fontFamily: 'Poppins', fontSize: 16.0),
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Text(
                      'Login',
                      style: TextStyle(
                          fontFamily: 'Poppins',
                          color: Color.fromRGBO(200, 141, 240, 1),
                          fontSize: 16.0,
                          fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 30.0,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
