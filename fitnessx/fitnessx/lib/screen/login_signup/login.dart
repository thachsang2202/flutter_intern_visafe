import 'dart:ffi';

import 'package:fitnessx/components/button.dart';
import 'package:fitnessx/components/input.dart';
import 'package:fitnessx/screen/login_signup/profile.dart';
import 'package:fitnessx/screen/login_signup/signup.dart';
import 'package:fitnessx/screen/login_signup/welcome.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool isChecked = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Column(
              children: [
                SizedBox(
                  height: 50.0,
                ),
                Text(
                  'Hey there,',
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w500,
                      fontSize: 20.0),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  'Welcome Back',
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w700,
                      fontSize: 25.0),
                ),
                SizedBox(
                  height: 25.0,
                ),
                Input(
                  icon: 'assets/login_signup/message_icon.svg',
                  placeHolder: 'Email',
                ),
                SizedBox(
                  height: 15.0,
                ),
                Input(
                  icon: 'assets/login_signup/lock_icon.svg',
                  placeHolder: 'Password',
                  iconEnd: 'assets/login_signup/hide_password.svg',
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Forgot your password?',
                  style: TextStyle(
                      fontSize: 18.0,
                      fontFamily: 'Poppins',
                      color: Color.fromRGBO(173, 164, 165, 1),
                      decoration: TextDecoration.underline),
                )
              ],
            ),
            Column(
              children: [
                Button(
                  text: 'Login',
                  startIcon: 'assets/login_signup/login_icon.svg',
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const WelCome()));
                  },
                ),
                const Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Divider(
                          height: 1.0,
                          indent: 2.0,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        child: Text(
                          'Or',
                          style: TextStyle(fontFamily: 'Poppins'),
                        ),
                      ),
                      Expanded(
                        child: Divider(
                          height: 1.0,
                        ),
                      )
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 70,
                      height: 70,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(15)),
                          border: Border.all(
                              width: 1.0,
                              color: const Color.fromRGBO(221, 218, 218, 1))),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20.0, vertical: 20.0),
                        child: Container(
                          decoration: const BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(
                                      'assets/login_signup/google_logo.png'))),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 50.0,
                    ),
                    Container(
                      width: 70,
                      height: 70,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(15)),
                          border: Border.all(
                              width: 1.0,
                              color: const Color.fromRGBO(221, 218, 218, 1))),
                      child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 20.0),
                          child: SvgPicture.asset(
                              'assets/login_signup/facebook_logo.svg')),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      'Already have an account?',
                      style: TextStyle(fontFamily: 'Poppins', fontSize: 16.0),
                    ),
                    const SizedBox(
                      width: 5.0,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const SignUp()));
                      },
                      child: const Text(
                        'Register',
                        style: TextStyle(
                            fontFamily: 'Poppins',
                            color: Color.fromRGBO(200, 141, 240, 1),
                            fontSize: 16.0,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 30.0,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
