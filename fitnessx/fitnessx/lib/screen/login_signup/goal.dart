import 'package:fitnessx/components/button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Goal extends StatefulWidget {
  final String image;
  final String goal;
  final String description;
  final Widget nextGoal;
  const Goal(
      {super.key,
      required this.image,
      required this.goal,
      required this.description,
      required this.nextGoal});

  @override
  State<Goal> createState() => _GoalState();
}

class _GoalState extends State<Goal> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 30.0,
            ),
            const Text(
              'What is your goal ?',
              style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w700,
                  fontSize: 25.0),
            ),
            const SizedBox(
              height: 10.0,
            ),
            const SizedBox(
              width: 270.0,
              child: Text(
                'It will help us to choose a best program for you',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Poppins',
                    color: Color.fromRGBO(123, 111, 114, 1),
                    fontSize: 15.0),
              ),
            ),
            const SizedBox(
              height: 30.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: 400.0,
                  width: 40.0,
                  decoration: const BoxDecoration(
                      color: Color.fromRGBO(223, 229, 254, 1),
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20.0),
                          bottomRight: Radius.circular(20.0))),
                ),
                Container(
                  height: 490.0,
                  width: 250.0,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      gradient: LinearGradient(colors: [
                        Color.fromRGBO(154, 196, 255, 1),
                        Color.fromRGBO(146, 165, 253, 1)
                      ])),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const SizedBox(
                        height: 30.0,
                      ),
                      SvgPicture.asset(widget.image),
                      const SizedBox(
                        height: 30.0,
                      ),
                      Text(
                        widget.goal,
                        style: const TextStyle(
                            fontFamily: 'Poppins',
                            color: Colors.white,
                            fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      const Divider(
                        height: 1.0,
                        indent: 100.0,
                        endIndent: 100.0,
                        color: Colors.white,
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      SizedBox(
                        width: 200,
                        child: Text(
                          widget.description,
                          style: const TextStyle(
                              fontFamily: 'Poppins',
                              color: Colors.white,
                              fontSize: 12.0),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 400.0,
                  width: 40.0,
                  decoration: const BoxDecoration(
                      color: Color.fromRGBO(225, 237, 255, 1),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.0),
                          bottomLeft: Radius.circular(20.0))),
                )
              ],
            ),
            const SizedBox(
              height: 50.0,
            ),
            Button(
                text: 'Confirm',
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => widget.nextGoal));
                })
          ],
        ),
      ),
    );
  }
}
