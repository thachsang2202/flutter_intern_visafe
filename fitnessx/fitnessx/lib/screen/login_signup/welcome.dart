import 'package:fitnessx/components/button.dart';
import 'package:fitnessx/screen/dashboard/dashboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class WelCome extends StatefulWidget {
  const WelCome({super.key});

  @override
  State<WelCome> createState() => _WelComeState();
}

class _WelComeState extends State<WelCome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SizedBox(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 50.0,
              ),
              SvgPicture.asset('assets/login_signup/welcome.svg'),
              const SizedBox(
                height: 20.0,
              ),
              const Text(
                'Welcome, Stefani',
                style: TextStyle(
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w700,
                    fontSize: 25.0),
              ),
              const SizedBox(
                height: 10.0,
              ),
              const SizedBox(
                width: 300.0,
                child: Text(
                  'You are all set now, let’s reach your goals together with us',
                  style: TextStyle(
                      fontFamily: 'Poppins',
                      color: Color.fromRGBO(123, 111, 114, 1),
                      fontSize: 15.0),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
          Container(
              margin: const EdgeInsets.symmetric(vertical: 30.0),
              child: Button(
                  text: 'Go to Home',
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => DashBoard()));
                  }))
        ],
      ),
    ));
  }
}
