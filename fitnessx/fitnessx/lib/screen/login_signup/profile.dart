import 'package:fitnessx/components/button.dart';
import 'package:fitnessx/components/input.dart';
import 'package:fitnessx/components/input_unit.dart';
import 'package:fitnessx/screen/login_signup/goal.dart';
import 'package:fitnessx/screen/login_signup/login.dart';
import 'package:fitnessx/screen/login_signup/signup.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
                width: MediaQuery.of(context).size.width,
                child: SvgPicture.asset(
                  'assets/login_signup/complete_profile.svg',
                  fit: BoxFit.fill,
                )),
            const SizedBox(
              height: 20.0,
            ),
            const Text(
              'Let’s complete your profile',
              style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w700,
                  fontSize: 25.0),
            ),
            const SizedBox(
              height: 10.0,
            ),
            const Text(
              'It will help us to know more about you!',
              style: TextStyle(
                  fontFamily: 'Poppins',
                  color: Color.fromRGBO(123, 111, 114, 1),
                  fontSize: 15.0),
            ),
            const SizedBox(
              height: 20.0,
            ),
            const Input(
              icon: 'assets/login_signup/2_user.svg',
              placeHolder: 'Chose gender',
              iconEnd: 'assets/login_signup/dropdown.svg',
            ),
            const SizedBox(
              height: 10.0,
            ),
            const Input(
              icon: 'assets/login_signup/calendar_icon.svg',
              placeHolder: 'Day of birth',
            ),
            const SizedBox(
              height: 10.0,
            ),
            const InputUnit(
                icon: 'assets/login_signup/weight_icon.svg',
                placeHolder: 'Your weight',
                unit: 'KG'),
            const SizedBox(
              height: 10.0,
            ),
            const InputUnit(
                icon: 'assets/login_signup/swap_icon.svg',
                placeHolder: 'Your height',
                unit: 'CM'),
            const SizedBox(
              height: 20.0,
            ),
            Button(
              text: 'Next',
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const Goal(
                              image: 'assets/login_signup/goal1.svg',
                              goal: 'Improve Shape',
                              description:
                                  'I have a low amount of body fat and need / want to build more muscle',
                              nextGoal: Goal(
                                image: 'assets/login_signup/goal2.svg',
                                goal: 'Lean & Tone',
                                description:
                                    'I’m “skinny fat”. look thin but have no shape. I want to add learn muscle in the right way',
                                nextGoal: Goal(
                                  image: 'assets/login_signup/goal3.svg',
                                  goal: 'Lose a Fat',
                                  description:
                                      'I have over 20 lbs to lose. I want to drop all this fat and gain muscle mass',
                                  nextGoal: Login(),
                                ),
                              ),
                            )));
              },
              endIcon: 'assets/login_signup/arrow_right.svg',
            )
          ],
        ),
      ),
    );
  }
}
