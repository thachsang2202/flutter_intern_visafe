import 'package:flutter/material.dart';

class CardValue extends StatefulWidget {
  final String value;
  final String title;
  const CardValue({super.key, required this.value, required this.title});

  @override
  State<CardValue> createState() => _CardState();
}

class _CardState extends State<CardValue> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      width: 100,
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.1),
                spreadRadius: 3,
                blurRadius: 7,
                offset: const Offset(0, 3))
          ],
          borderRadius: const BorderRadius.all(Radius.circular(15.0))),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              widget.value,
              style: const TextStyle(
                  color: Color.fromRGBO(150, 176, 254, 1),
                  fontFamily: 'Poppins',
                  fontSize: 18.0,
                  fontWeight: FontWeight.w500),
            ),
            Text(
              widget.title,
              style: const TextStyle(
                  color: Color.fromRGBO(123, 111, 114, 1),
                  fontFamily: 'Poppins',
                  fontSize: 15.0,
                  fontWeight: FontWeight.w500),
            )
          ],
        ),
      ),
    );
  }
}
