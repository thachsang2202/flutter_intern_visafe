import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Button extends StatefulWidget {
  final String text;
  final Function onPressed;
  final String? endIcon;
  final String? startIcon;

  const Button(
      {super.key,
      required this.text,
      required this.onPressed,
      this.startIcon,
      this.endIcon});

  @override
  State<Button> createState() => _ButtonState();
}

class _ButtonState extends State<Button> {
  bool _isTapped = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (_) {
        setState(() {
          _isTapped = true;
        });
        widget.onPressed();
      },
      onTapUp: (_) {
        setState(() {
          _isTapped = false;
        });
      },
      onTapCancel: () {
        setState(() {
          _isTapped = false;
        });
      },
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [
                  !_isTapped
                      ? const Color.fromRGBO(154, 196, 254, 1)
                      : const Color.fromRGBO(154, 196, 254, 0.5),
                  !_isTapped
                      ? const Color.fromRGBO(146, 164, 253, 1)
                      : const Color.fromRGBO(146, 164, 253, 0.5)
                ]),
            borderRadius: const BorderRadius.all(Radius.circular(50))),
        height: 50.0,
        width: 350.0,
        child: Center(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            widget.startIcon != null
                ? SvgPicture.asset(widget.startIcon!)
                : SizedBox(),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Text(
                widget.text,
                style: const TextStyle(
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w600,
                    fontSize: 18.0,
                    color: Colors.white),
              ),
            ),
            widget.endIcon != null
                ? SvgPicture.asset(widget.endIcon!)
                : SizedBox()
          ],
        )),
      ),
    );
  }
}
