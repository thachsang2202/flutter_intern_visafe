import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Workout extends StatefulWidget {
  final Color backgroundColor;
  final String title;
  final int calor;
  final int minutes;
  final double percent;
  final String? image;
  const Workout(
      {super.key,
      required this.backgroundColor,
      required this.title,
      required this.calor,
      required this.minutes,
      required this.percent,
      this.image});

  @override
  State<Workout> createState() => _WorkoutState();
}

class _WorkoutState extends State<Workout> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: 400,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(15.0)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 1,
            blurRadius: 5,
            offset: Offset(0, 2),
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                color: widget.backgroundColor,
                borderRadius: const BorderRadius.all(Radius.circular(30)),
              ),
              child: widget.image != null
                  ? SvgPicture.asset(widget.image!)
                  : const SizedBox(),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  widget.title,
                  style: const TextStyle(
                      fontFamily: 'Poppins', fontWeight: FontWeight.w500),
                ),
                Text(
                  '${widget.calor} Calories Burn | ${widget.minutes} minutes',
                  style:
                      const TextStyle(color: Color.fromRGBO(164, 169, 173, 1)),
                ),
                Stack(
                  children: [
                    Container(
                      width: 220,
                      height: 20,
                      decoration: const BoxDecoration(
                          color: Color.fromRGBO(247, 248, 248, 1),
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                    ),
                    Container(
                      width: widget.percent * 220 / 100,
                      height: 20,
                      decoration: const BoxDecoration(
                          gradient: LinearGradient(colors: [
                            Color.fromRGBO(146, 163, 253, 1),
                            Color.fromRGBO(197, 139, 242, 1)
                          ]),
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                    ),
                  ],
                )
              ],
            ),
            SvgPicture.asset(
              'assets/dashboard/circle.svg',
              height: 40,
              width: 40,
            )
          ],
        ),
      ),
    );
  }
}
