import 'package:flutter/material.dart';

class Progress extends StatefulWidget {
  final String title;
  final bool isTypeColorLight;
  final double percent;
  const Progress(
      {super.key,
      required this.title,
      required this.isTypeColorLight,
      required this.percent});

  @override
  State<Progress> createState() => _ProgressState();
}

class _ProgressState extends State<Progress> {
  double maxHeight = 180;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          children: [
            Container(
              width: 30,
              height: maxHeight,
              decoration: const BoxDecoration(
                  color: Color.fromRGBO(247, 248, 248, 1),
                  borderRadius: BorderRadius.all(Radius.circular(15))),
            ),
            Positioned(
              bottom: 0,
              child: Container(
                width: 30,
                height: widget.percent * maxHeight / 100,
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      widget.isTypeColorLight
                          ? const Color.fromRGBO(157, 206, 255, 1)
                          : const Color.fromRGBO(238, 164, 206, 1),
                      widget.isTypeColorLight
                          ? const Color.fromRGBO(146, 163, 253, 1)
                          : const Color.fromRGBO(197, 139, 242, 1)
                    ]),
                    borderRadius: const BorderRadius.all(Radius.circular(15))),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        Text(
          widget.title,
          style: const TextStyle(
              fontFamily: 'Poppins', color: Color.fromRGBO(123, 111, 114, 1)),
        )
      ],
    );
  }
}
