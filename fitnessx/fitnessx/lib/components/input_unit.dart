import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class InputUnit extends StatefulWidget {
  final String icon;
  final String placeHolder;
  final String unit;
  const InputUnit(
      {super.key,
      required this.icon,
      required this.placeHolder,
      required this.unit});

  @override
  State<InputUnit> createState() => _InputUnitState();
}

class _InputUnitState extends State<InputUnit> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              height: 50.0,
              decoration: const BoxDecoration(
                  color: Color.fromRGBO(247, 248, 248, 1),
                  borderRadius: BorderRadius.all(Radius.circular(15.0))),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: SvgPicture.asset(
                      widget.icon,
                      // ignore: deprecated_member_use
                      color: const Color.fromRGBO(123, 111, 114, 1),
                      height: 20,
                      width: 20,
                    ),
                  ),
                  Expanded(
                    child: TextField(
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: widget.placeHolder,
                          hintStyle: const TextStyle(
                              fontFamily: 'Poppins',
                              color: Color.fromRGBO(173, 164, 165, 1),
                              fontSize: 18)),
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            width: 10.0,
          ),
          Container(
            height: 50.0,
            width: 50.0,
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
                gradient: LinearGradient(colors: [
                  Color.fromRGBO(228, 158, 214, 1),
                  Color.fromRGBO(199, 140, 240, 1)
                ], begin: Alignment.centerLeft, end: Alignment.centerRight)),
            child: Center(
              child: Text(
                widget.unit,
                style: const TextStyle(
                    fontFamily: 'Poppins',
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
            ),
          )
        ],
      ),
    );
  }
}
