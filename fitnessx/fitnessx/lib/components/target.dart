import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Target extends StatefulWidget {
  final String value;
  final String title;
  final String image;

  const Target(
      {super.key,
      required this.value,
      required this.title,
      required this.image});

  @override
  State<Target> createState() => _TargetState();
}

class _TargetState extends State<Target> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: [
            SvgPicture.asset(
              widget.image,
              height: 40,
            ),
            const SizedBox(
              width: 10.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  widget.value,
                  style: const TextStyle(
                      color: Color.fromRGBO(150, 176, 254, 1),
                      fontFamily: 'Poppins',
                      fontSize: 18.0,
                      fontWeight: FontWeight.w500),
                ),
                Text(
                  widget.title,
                  style: const TextStyle(
                      color: Color.fromRGBO(123, 111, 114, 1),
                      fontFamily: 'Poppins',
                      fontSize: 15.0,
                      fontWeight: FontWeight.w500),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
