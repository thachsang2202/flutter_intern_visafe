import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ActivityCard extends StatefulWidget {
  final String image;
  final String title;
  final String time;

  const ActivityCard(
      {super.key,
      required this.image,
      required this.title,
      required this.time});

  @override
  State<ActivityCard> createState() => _ActivityCardState();
}

class _ActivityCardState extends State<ActivityCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10.0),
      width: 400,
      height: 100,
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                spreadRadius: 3,
                blurRadius: 7,
                offset: const Offset(0, 3))
          ],
          borderRadius: const BorderRadius.all(Radius.circular(15.0))),
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          SizedBox(
            height: 60,
            width: 60,
            child: SvgPicture.asset(
              widget.image,
              height: 10,
              width: 10,
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  widget.title,
                  style: const TextStyle(
                      fontFamily: 'Poppins', fontWeight: FontWeight.w600),
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  widget.time,
                  style: const TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(164, 169, 173, 1)),
                )
              ],
            ),
          ),
          SvgPicture.asset(
            'assets/dashboard/more_icon.svg',
            height: 20,
            width: 20,
          )
        ],
      ),
    );
  }
}
