import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';

class NotificationCard extends StatefulWidget {
  final String? image;
  final Color backgroundColor;
  final String title;
  final String time;

  const NotificationCard(
      {super.key,
      this.image,
      required this.backgroundColor,
      required this.title,
      required this.time});

  @override
  State<NotificationCard> createState() => _NotificationCardState();
}

class _NotificationCardState extends State<NotificationCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      width: 400,
      decoration: const BoxDecoration(
          border: Border(
              bottom: BorderSide(color: Color.fromRGBO(221, 218, 218, 1)))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Container(
                width: 60,
                height: 60,
                decoration: BoxDecoration(
                  color: widget.backgroundColor,
                  borderRadius: const BorderRadius.all(Radius.circular(30)),
                ),
                child: widget.image != null
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SvgPicture.asset(widget.image!),
                      )
                    : const SizedBox(),
              ),
              const SizedBox(
                width: 15,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    widget.title,
                    style: const TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 18.0,
                        fontWeight: FontWeight.w500),
                  ),
                  Text(
                    widget.time,
                    style: const TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 16.0,
                        color: Color.fromRGBO(123, 111, 114, 1),
                        fontWeight: FontWeight.w500),
                  )
                ],
              )
            ],
          ),
          SvgPicture.asset(
            'assets/dashboard/more_icon.svg',
            height: 20,
            width: 20,
          )
        ],
      ),
    );
  }
}
