import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Section extends StatefulWidget {
  final String icon;
  final String title;
  const Section({super.key, required this.icon, required this.title});

  @override
  State<Section> createState() => _SectionState();
}

class _SectionState extends State<Section> {
  @override
  Widget build(BuildContext context) {
    return Row(children: [
      SvgPicture.asset(widget.icon),
      const SizedBox(
        width: 15.0,
      ),
      Expanded(
        child: Text(
          widget.title,
          style: const TextStyle(
              color: Color.fromRGBO(123, 111, 114, 1),
              fontFamily: 'Poppins',
              fontSize: 15.0,
              fontWeight: FontWeight.w500),
        ),
      ),
      SvgPicture.asset('assets/dashboard/arrow_right.svg'),
    ]);
  }
}
