import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NavBar extends StatelessWidget {
  final int pageIndex;
  final Function(int) onTap;

  const NavBar({
    super.key,
    required this.pageIndex,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BottomAppBar(
        elevation: 0.0,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Container(
            height: 60,
            color: Colors.white,
            child: Row(
              children: [
                navItem(
                  'assets/dashboard/home_icon.svg',
                  pageIndex == 0,
                  onTap: () => onTap(0),
                ),
                navItem(
                  'assets/dashboard/activity_icon.svg',
                  pageIndex == 1,
                  onTap: () => onTap(1),
                ),
                const SizedBox(width: 80),
                navItem(
                  'assets/dashboard/camera_icon.svg',
                  pageIndex == 2,
                  onTap: () => onTap(2),
                ),
                navItem(
                  'assets/dashboard/profile_icon.svg',
                  pageIndex == 3,
                  onTap: () => onTap(3),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget navItem(String icon, bool selected, {Function()? onTap}) {
    return Expanded(
      child: InkWell(
          onTap: onTap,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                icon,
                color: selected
                    ? Color.fromRGBO(214, 150, 227, 1)
                    : Color.fromRGBO(173, 164, 165, 1),
              ),
              SizedBox(
                height: 2.0,
              ),
              Container(
                height: 5,
                width: 5,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15.0)),
                  color: selected
                      ? Color.fromRGBO(214, 150, 227, 1)
                      : Colors.white,
                ),
              )
            ],
          )),
    );
  }
}
