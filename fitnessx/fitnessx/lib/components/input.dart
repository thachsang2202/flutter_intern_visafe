import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class Input extends StatefulWidget {
  final String icon;
  final String placeHolder;
  final String? iconEnd;

  const Input(
      {super.key, required this.icon, required this.placeHolder, this.iconEnd});

  @override
  State<Input> createState() => _InputState();
}

class _InputState extends State<Input> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.0,
      width: 350.0,
      decoration: const BoxDecoration(
          color: Color.fromRGBO(247, 248, 248, 1),
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: SvgPicture.asset(
              widget.icon,
              // ignore: deprecated_member_use
              color: const Color.fromRGBO(123, 111, 114, 1),
              height: 20,
              width: 20,
            ),
          ),
          Expanded(
            child: TextField(
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: widget.placeHolder,
                  hintStyle: const TextStyle(
                      fontFamily: 'Poppins',
                      color: Color.fromRGBO(173, 164, 165, 1),
                      fontSize: 18)),
            ),
          ),
          widget.iconEnd != null
              ? Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: SvgPicture.asset(
                    widget.iconEnd!,
                    color: const Color.fromRGBO(123, 111, 114, 1),
                    height: 20,
                    width: 20,
                  ),
                )
              : const SizedBox()
        ],
      ),
    );
  }
}
