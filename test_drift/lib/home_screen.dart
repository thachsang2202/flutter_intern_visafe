import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:test_drift/data.dart';
import 'package:drift/drift.dart' as drift;

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final TextEditingController titleController = TextEditingController();
  final TextEditingController descriptController = TextEditingController();
  late Database _db;
  int? selectIndex;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _db = Database();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Products'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              decoration: const BoxDecoration(
                  color: Color.fromARGB(255, 221, 219, 219),
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              child: TextFormField(
                controller: titleController,
                cursorColor: Colors.green,
                decoration: const InputDecoration(
                  hintText: 'Title',
                  border: InputBorder.none,
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              decoration: const BoxDecoration(
                  color: Color.fromARGB(255, 221, 219, 219),
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              child: TextFormField(
                controller: descriptController,
                cursorColor: Colors.green,
                decoration: const InputDecoration(
                  hintText: 'Description',
                  border: InputBorder.none,
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    final entity = ProductsCompanion(
                        title: drift.Value(titleController.text),
                        description: drift.Value(descriptController.text));

                    _db.insertProduct(entity);
                  },
                  child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 30, vertical: 7),
                      decoration: const BoxDecoration(
                          color: Color.fromARGB(255, 13, 179, 38),
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                      child: const Text('Thêm')),
                ),
                const SizedBox(
                  width: 20,
                ),
                GestureDetector(
                  onTap: () {
                    if (selectIndex != null) {
                      final entity = ProductsCompanion(
                          id: drift.Value(selectIndex!),
                          title: drift.Value(titleController.text),
                          description: drift.Value(descriptController.text));
                      _db.updateProduct(entity);
                    }
                  },
                  child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 30, vertical: 7),
                      decoration: const BoxDecoration(
                          color: Color.fromARGB(255, 13, 179, 38),
                          borderRadius: BorderRadius.all(Radius.circular(15))),
                      child: const Text('Sửa')),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            StreamBuilder(
                stream: _db.getProducts(),
                builder: (_, snapshot) {
                  final List<Product>? products = snapshot.data;

                  if (snapshot.hasError) {
                    return Text(snapshot.error.toString());
                  } else if (snapshot.connectionState ==
                      ConnectionState.waiting) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    if (products != null) {
                      return Expanded(
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: products.length,
                          itemBuilder: (_, index) {
                            final product = products[index];
                            return GestureDetector(
                              onTap: () {
                                titleController.text = product.title;
                                descriptController.text = product.description;
                                selectIndex = product.id;
                              },
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        product.title,
                                        style: const TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      Text(product.description),
                                      const SizedBox(
                                        height: 5,
                                      )
                                    ],
                                  ),
                                  IconButton(
                                      onPressed: () {
                                        _db.deleteProduct(product.id);
                                      },
                                      icon: const Icon(Icons.delete))
                                ],
                              ),
                            );
                          },
                        ),
                      );
                    } else {
                      return const SizedBox();
                    }
                  }
                })
          ],
        ),
      ),
    );
  }
}
