import 'dart:async';
import 'dart:io';

import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';

part 'data.g.dart';

class Products extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get title => text()();
  TextColumn get description => text()();
}

abstract class ProductView extends View {
  Products get products;

  @override
  Query as() => select([products.title]).from(products);
}

LazyDatabase _openConnection() {
  return LazyDatabase(() async {
    final dbFolder = await getApplicationCacheDirectory();
    final file = File(path.join(dbFolder.path, 'product.sqlite'));

    return NativeDatabase(file);
  });
}

@DriftDatabase(tables: [Products], views: [ProductView])
class Database extends _$Database {
  Database() : super(_openConnection());

  @override
  int get schemaVersion => 2;

  Stream<List<Product>> getProducts() {
    final controller =
        StreamController<List<Product>>(); // Tạo một StreamController
    final query =
        select(products).watch(); // Lấy truy vấn watch() thay vì get()

    query.listen((event) {
      controller.add(event); // Khi có sự kiện, đẩy dữ liệu vào StreamController
    });

    return controller.stream; // Trả về stream từ controll
  }

  Stream<Product> getProduct(int id) {
    final controller = StreamController<Product>(); // Tạo một StreamController
    final query = (select(products)..where((tbl) => tbl.id.equals(id)))
        .watch(); // Lấy truy vấn watch() thay vì getSingle()

    query.listen((event) {
      if (event.isNotEmpty) {
        controller.add(event
            .first); // Nếu có kết quả, đẩy kết quả đầu tiên vào StreamController
      } else {
        controller.addError(
            "Product with id $id not found"); // Nếu không có kết quả, đẩy lỗi vào StreamController
      }
    });

    return controller.stream; // Trả về stream từ controller
  }

  Future<bool> updateProduct(ProductsCompanion entity) async {
    return await update(products).replace(entity);
  }

  Future<int> insertProduct(ProductsCompanion entity) async {
    return await into(products).insert(entity);
  }

  Future<int> deleteProduct(int id) async {
    return await (delete(products)..where((tbl) => tbl.id.equals(id))).go();
  }
}
