import 'package:floor/floor.dart';
import 'package:floor_project/dao/employee_dao.dart';
import 'package:floor_project/entity/employee.dart';
import 'dart:async';
import 'package:sqflite/sqflite.dart' as sqflite;

part 'database.g.dart';

@Database(version: 1, entities: [Employee])
abstract class AppDatabase extends FloorDatabase {
  EmployeeDAO get employeeDAO;
}
