import 'package:faker/faker.dart';
import 'package:floor_project/dao/employee_dao.dart';
import 'package:floor_project/entity/employee.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class HomeScreen extends StatefulWidget {
  final EmployeeDAO employeeDAO;
  const HomeScreen({super.key, required this.employeeDAO});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Emplyees'),
        actions: [
          IconButton(
              onPressed: () async {
                final employee = Employee(
                    id: DateTime.now().toString(),
                    firstName: Faker().person.firstName(),
                    lastName: Faker().person.lastName(),
                    email: Faker().internet.email());
                await widget.employeeDAO.insertEmployee(employee);
              },
              icon: const Icon(Icons.add)),
          IconButton(
              onPressed: () async {
                await widget.employeeDAO.deleteAllEmployee();
              },
              icon: const Icon(Icons.clear))
        ],
      ),
      body: StreamBuilder(
        stream: widget.employeeDAO.getAllEmployee(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text('${snapshot.error}'),
            );
          } else if (snapshot.hasData) {
            var listEmployee = snapshot.data as List<Employee>;
            return ListView.builder(
                itemCount: listEmployee.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                                '${listEmployee[index].firstName} ${listEmployee[index].lastName}'),
                            Text('${listEmployee[index].email}'),
                          ],
                        ),
                        Row(
                          children: [
                            IconButton(
                              icon: const Icon(Icons.update),
                              onPressed: () async {
                                final employee = Employee(
                                    id: listEmployee[index].id,
                                    firstName: Faker().person.firstName(),
                                    lastName: Faker().person.lastName(),
                                    email: Faker().internet.email());

                                await widget.employeeDAO
                                    .updateEmployee(employee);
                              },
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            IconButton(
                              icon: const Icon(Icons.delete),
                              onPressed: () async {
                                await widget.employeeDAO
                                    .deleteEmplyee(listEmployee[index]);
                              },
                            )
                          ],
                        )
                      ],
                    ),
                  );
                });
          } else {
            return const CircularProgressIndicator();
          }
        },
      ),
    );
  }
}
