import 'package:floor/floor.dart';

@entity
class Employee {
  @PrimaryKey()
  final String id;

  String firstName;
  String lastName;
  String email;

  Employee(
      {required this.id,
      required this.firstName,
      required this.lastName,
      required this.email});
}
