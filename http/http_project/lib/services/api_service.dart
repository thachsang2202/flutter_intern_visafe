import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http_project/models/post.dart';

class ApiService {
  Future<List<Post>> fetchPost() async {
    final respones =
        await http.get(Uri.parse('https://jsonplaceholder.typicode.com/posts'));

    if (respones.statusCode == 200) {
      final List<dynamic> data = json.decode(respones.body);
      return data.map((json) => Post.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load data');
    }
  }
}
