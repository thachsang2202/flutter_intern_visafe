import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:retrofit_project/models/post.dart';

part 'api_service.g.dart';

@RestApi(baseUrl: 'https://jsonplaceholder.typicode.com/')
abstract class ApiService {
  factory ApiService(Dio dio) = _ApiService;

  @GET('/posts')
  Future<List<Post>> getPosts();
}
